# nolfet

Firefox addon that un-sticks sticky and fixed elements:

https://addons.mozilla.org/en-US/firefox/addon/nolfet/

Forked from [Unstick!](https://addons.mozilla.org/en-US/firefox/addon/unstickall/).

The only difference is that, while Unstick! deletes the offending element,
nolfet changes its position styling to 'static'.

# TODO:

There are a number of improvements to be made to this add-on, including the following:

- site whitelist
- toggle switch
- better name/logo 
  - I'm thinking 'no felt'
  - as in, the felt boards that they use in bible school
  - - also it's an anagram for our favorite non-stick substance