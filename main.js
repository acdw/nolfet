// forked from UnStick! 
// (https://addons.mozilla.org/en-US/firefox/addon/unstickall/)

var bannedPositions = [
    "fixed", "sticky", "-webkit-sticky", "-moz-sticky",
    "-ms-sticky", "-o-sticky"
];
var bannedIds = ["fixedScroll"];

function isStuck(element) {
    // check if element id matches banned id
    if (bannedIds.indexOf(element.id) > -1) { return true; }

    // normalize the possible positions
    let positions = [
        element.style.position, 
        window.getComputedStyle(element).position]
            .map(p => p.toLowerCase().replace("!important", "").trim());

    // check for position: absolute + top: *px 
    // (likely stuck with javascript)
    if (positions.indexOf("absolute") > -1 
        && element.style.top.toLowerCase().indexOf("px") > -1) {
        return true;
    }
    
    // check if element position matches banned position
    return positions
        .map(p => bannedPositions.indexOf(p) > -1)
        .reduce((a, b) => a ? a : b);
}

function unstick(arr) {
    // check every element, and unstick from screen
    // here's where, instead of deleting the element,
    // I just make the position 'absolute'
    arr
        .filter(e => isStuck(e))
        .forEach(e => e.style.position = "static");
        //.forEach(e => e.parentNode.removeChild(e));
}

unstick(Array.from(document.querySelectorAll('body *')))
